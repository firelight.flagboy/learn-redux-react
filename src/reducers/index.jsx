import { ADD_ARTICLE } from '../constants/action-types.jsx'

const initialState = {
	articles: []
};

function rootReducer(state = initialState, action) {
	switch (action.type) {
		case ADD_ARTICLE:
			const { articles } = state
			return { ...state, articles: [...articles, action.payload] }
		default:
			return state;
	}
}

export default rootReducer;
