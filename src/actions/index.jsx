import { ADD_ARTICLE } from '../constants/action-types.jsx';

export const addArticle = function (article) {
	return {
		type: ADD_ARTICLE,
		payload: article
	}
}
